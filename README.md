# ValheimTexturePack

Een meme van een texture pack voor Valheim via deze mod [linkje](https://www.nexusmods.com/valheim/mods/48?tab=description)

In the [scene_dump.txt](https://gitlab.com/GitGeert/valheimtexturepack/-/blob/main/scene_dump.txt) file staan alle texture namen en properties.
```
-Valheimserver
```
# How to installeren deze modpack op Persoonlijke Computer?

- [ ] Installeer Valheim via Steam
- [ ] Copy paste de inhoud van [ValheimMods.zip](https://gitlab.com/GitGeert/valheimtexturepack/-/blob/main/ValheimMods.zip) in ``` ../Steam/steamapps/common/Valheim ```
- [ ] Launch de game en check of BepInEx klaagt in de command window die launched met Valheim
- [ ] Reboot Valheim
- [ ] copy paste [deze folder](https://gitlab.com/GitGeert/valheimtexturepack/-/tree/main/EditedTextures) in BepInEx/Plugins/CustomTextures om de texture pack te installeren
- [ ] Join op radohm.nl (je kunt IP safen met Valheim+)

# Alternative Install Method With Vortex (not tested)

- [ ] Download Valheim en Vortex [mod manager](https://www.nexusmods.com/about/vortex/).
- [ ] Installeer de Valheim Plus mod [linkje](https://www.nexusmods.com/valheim/mods/4).
- [ ] Installeer de custom texture pack mod [linkje](https://www.nexusmods.com/valheim/mods/48).
	- [ ] copy paste [deze folder](https://gitlab.com/GitGeert/valheimtexturepack/-/tree/main/EditedTextures) in BepInEx/Plugins/CustomTextures om de texture pack te installeren
	- [ ] voordat deze folder is generate moet je waarschijnlijk eerst Valheim een keer runnen en restarten
- [ ] Installeer de custom loading screen mod [linkje](https://www.nexusmods.com/valheim/mods/553).

# Current Textures


# Workflow

- [ ] Verzin een texture die je wilt veranderen.
- [ ] Check scene_dump.txt. (Set dump scene aan in config van custom texture mod)
- [ ] Zoek je texture op. (hint zet hele woorden uit voor ctrl+f pogingen)
- [ ] Pour Example a la greydwarf: 
```
			object Greydwarf has 4 SkinnedMeshRenderers:
				SkinnedMeshRenderer name: Cube
					smr Cube has 1 materials
						greydwarf (Instance):
							properties:
								_MainTex greydrawrf_diffuse
								_BumpMap greydrawrf_diffuse_nrm
								_EmissionMap 
								_MetallicGlossMap 
								_StyleTex 
				SkinnedMeshRenderer name: Cube.001
					smr Cube.001 has 1 materials
						greydwarf (Instance):
							properties:
								_MainTex greydrawrf_diffuse
								_BumpMap greydrawrf_diffuse_nrm
								_EmissionMap 
								_MetallicGlossMap 
								_StyleTex 
```
- [ ] Dus als je de greydwarf texture wilt veranderen heb je de greydrawrf_diffuse.png file nodig die je kunt krijgen met een Unity Asset Editor [die is hier te vinden](https://ci.appveyor.com/project/Perfare/assetstudio/branch/master/artifacts)
- [ ] Er is ook een BumpMap naast de main texture png. Ik heb vernomen dat die soort van een diepte aangeeft en dat je die met GIMP kan maken.
- [ ] Edit de png met je favoriete programma. (hint gebruik Paint3D)
- [ ] Geef de nieuwe .png file een naam according tot wat ie moet zijn.
		```Pour Example: objectrenderer_Greydwarf_Cube_MainTex.png"```
- [ ] Zet je nieuwe texture in je game files onder `steam/steamapps/common/Valheim/BepInEx/Plugins/CustomTextures`
- [ ] Launch game in test world of een world waar je niet veel omgeeft, zou zonde zijn als alles stukging.
- [ ] Als je mod eenmaal 1 keer ingeladen hebt kun je "pg dn" gebruiken om alle textures opnieuw in te laden. Dit zorgt ervoor dat je niet steeds je game opnieuw opstart.
# FAQ

## Hoe werken Textures eigenlijk?

Da's een goeie geen idee.
Maar hier is de greydwarf_diffuse texture als voorbeeld :)

![GreyDwarf](Texture2D/greydrawrf_diffuse.png)

## Waarom staan er overal vierkantjes in deze readme?

[ ] Waarom zijn bananen krom wat is dit voor vraag?

## Waarom is greydrawf als greydrawrf gespelt in scene_dump.txt?

Goeie er zitten meer typfouten in de scene_dump.txt toch zul je het er maar mee moeten doen!

## 

